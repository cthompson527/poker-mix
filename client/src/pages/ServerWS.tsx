import React, { useState } from 'react';
import { withStyles, createStyles } from '@material-ui/core/styles';
import handRankings from '../images/hand-rankings.jpg';
import { subscribeAsServer } from '../websocket';

const styles = createStyles({
  root: {
    marginTop: '15px',
  },
  title: {
    paddingTop: '25px',
    paddingBottom: '25px',
    alignSelf: 'center',
  },
  button: {
    alignSelf: 'center',
    width: '95%',
    marginTop: '10px',
  },
  media: {
    height: '650px',
    backgroundImage: `url(${handRankings})`,
    backgroundSize: 'contain',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
  },
  mTop: {
    marginTop: '10px',
  },
  mRight: {
    marginRight: '10px',
  },
  mLeft: {
    marginLeft: '10px',
  },
});
const connectToWS = (history: History) => subscribeAsServer(history);

const C = (props: any) => {
  const [connected, setConnected] = useState(false);
  if (!connected) {
    console.log('connecting');
    connectToWS(props.history);
    setConnected(true);
  }
  return <div />;
};

export default withStyles(styles)(C);
