import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { withStyles, createStyles, WithStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import texasHoldem from '../images/texas-holdem.jpg';
import classNames from 'classnames';

const styles = createStyles({
  root: {
    marginTop: '15px',
  },
  title: {
    paddingTop: '25px',
    paddingBottom: '25px',
    alignSelf: 'center',
  },
  button: {
    alignSelf: 'center',
    width: '95%',
    marginTop: '10px',
  },
  media: {
    height: '700px',
    backgroundImage: `url(${texasHoldem})`,
    backgroundSize: 'contain',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    padding: '-5px',
  },
  mTop: {
    marginTop: '10px',
  },
  mRight: {
    marginRight: '10px',
  },
  mLeft: {
    marginLeft: '10px',
  },
  description: {
    height: '700px',
  },
});

const C = (props: WithStyles<typeof styles>) => {
  return (
    <Grid container justify="center" className={props.classes.root}>
      <Grid item xs={11}>
        <Card elevation={4} className={props.classes.title}>
          <Typography component="h2" variant="h3" align="center">
            🃏 Texas Hold'em Bomb Pot 🃏
          </Typography>
          <Typography component="p" variant="display1" color="textSecondary">
            <i>"If you can’t beat them, bluff them!" ~ Kudelis</i>
          </Typography>
        </Card>
      </Grid>
      <Grid item xs={5}>
        <Card
          className={classNames(
            props.classes.mTop,
            props.classes.mRight,
            props.classes.description,
          )}
          elevation={4}
        >
          <CardContent>
            <Typography component="h4" variant="display2">
              Blind $0.50
            </Typography>
            <Typography component="p" variant="display1" align="left">
              Two cards, known as hole cards, are dealt face down to each
              player. As a "bomb pot", we go straight to the flop and the three
              cards are dealt immediately. Everything afterwards is treated the
              same as Texas Holdem.
            </Typography>
          </CardContent>
        </Card>
      </Grid>
      <Grid item xs={6}>
        <Card
          className={classNames(
            props.classes.media,
            props.classes.mTop,
            props.classes.mLeft,
          )}
          elevation={4}
        />
      </Grid>
    </Grid>
  );
};

export default withStyles(styles)(C);
