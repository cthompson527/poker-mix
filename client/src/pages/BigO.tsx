import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { withStyles, createStyles, WithStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import omaha from '../images/omaha.jpg';
import classNames from 'classnames';

const styles = createStyles({
  root: {
    marginTop: '15px',
  },
  title: {
    paddingTop: '25px',
    paddingBottom: '25px',
    alignSelf: 'center',
  },
  button: {
    alignSelf: 'center',
    width: '95%',
    marginTop: '10px',
  },
  media: {
    height: '700px',
    backgroundImage: `url(${omaha})`,
    backgroundSize: 'contain',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    padding: '-5px',
  },
  mTop: {
    marginTop: '10px',
  },
  mRight: {
    marginRight: '10px',
  },
  mLeft: {
    marginLeft: '10px',
  },
  description: {
    height: '700px',
  },
});

const C = (props: WithStyles<typeof styles>) => {
  return (
    <Grid container justify="center" className={props.classes.root}>
      <Grid item xs={11}>
        <Card elevation={4} className={props.classes.title}>
          <Typography component="h2" variant="h3" align="center">
            🃏 Big O 🃏
          </Typography>
          <Typography component="p" variant="display1" color="textSecondary">
            <i>"I told Worm you can't lose what you don't put in the middle.  But you can't win much either." ~ Mike McDermott (Rounders)</i>
          </Typography>
        </Card>
      </Grid>
      <Grid item xs={5}>
        <Card
          className={classNames(
            props.classes.mTop,
            props.classes.mRight,
            props.classes.description,
          )}
          elevation={4}
        >
          <CardContent>
            <Typography component="h4" variant="display2">
              Blinds $0.05/$0.10
            </Typography>
            <Typography component="p" variant="display1" align="left">
              <ul>
                <li>Each player is dealt five cards face down</li>
                <li>The rest of the game is played exactly like PLO8</li>
                <li>The max number of players able to play Big O is 9, and the dealer does not burn a card prior to dealing the river if 9 players are seated</li>
              </ul>
            </Typography>
          </CardContent>
        </Card>
      </Grid>
      <Grid item xs={6}>
        <Card
          className={classNames(
            props.classes.media,
            props.classes.mTop,
            props.classes.mLeft,
          )}
          elevation={4}
        />
      </Grid>
    </Grid>
  );
};

export default withStyles(styles)(C);
