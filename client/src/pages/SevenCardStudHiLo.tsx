import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { withStyles, createStyles, WithStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import sevenCardStud from '../images/7cardstud.jpg';
import classNames from 'classnames';

const styles = createStyles({
  root: {
    marginTop: '15px',
  },
  title: {
    paddingTop: '25px',
    paddingBottom: '25px',
    alignSelf: 'center',
  },
  button: {
    alignSelf: 'center',
    width: '95%',
    marginTop: '10px',
  },
  media: {
    height: '700px',
    backgroundImage: `url(${sevenCardStud})`,
    backgroundSize: 'contain',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    padding: '-5px',
  },
  mTop: {
    marginTop: '10px',
  },
  mRight: {
    marginRight: '10px',
  },
  mLeft: {
    marginLeft: '10px',
  },
  description: {
    height: '700px',
  },
});

const C = (props: WithStyles<typeof styles>) => {
  return (
    <Grid container justify="center" className={props.classes.root}>
      <Grid item xs={11}>
        <Card elevation={4} className={props.classes.title}>
          <Typography component="h2" variant="h3" align="center">
            🃏 Seven Card Stud Hi/Lo 🃏
          </Typography>
          <Typography component="p" variant="display1" color="textSecondary" style={{ fontSize: '1.8em' }}>
            <i>"If you can't spot the sucker in your first half hour at the table, then you are the sucker." ~ Mike McDermott (Rounders)</i>
          </Typography>
        </Card>
      </Grid>
      <Grid item xs={5}>
        <Card
          className={classNames(
            props.classes.mTop,
            props.classes.mRight,
            props.classes.description,
          )}
          elevation={4}
        >
          <CardContent>
            <Typography component="h4" variant="display2">
              $0.05/$0.10/$0.25/$0.50
            </Typography>
            <Typography component="p" variant="display1" align="left">
              <ul>
                <li>Stud 8 is a split pot variation where the lowest qualifying hand wins half the pot</li>
                <li>A qualifying low hand is five cards of the following criteria:</li>
                <ul>
                  <li>No pairs</li>
                  <li>Aces are always low</li>
                  <li>All five cards are 8 or below</li>
                </ul>
                <li>Straights or flushes do not count against players. Therefore, the best possible low hand is A2345 and the worst is 87654</li>
              </ul>
            </Typography>
          </CardContent>
        </Card>
      </Grid>
      <Grid item xs={6}>
        <Card
          className={classNames(
            props.classes.media,
            props.classes.mTop,
            props.classes.mLeft,
          )}
          elevation={4}
        />
      </Grid>
    </Grid>
  );
};

export default withStyles(styles)(C);
