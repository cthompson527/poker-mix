import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { withStyles, createStyles, WithStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import fiveCardStud from '../images/5cardstud.jpg';
import classNames from 'classnames';

const styles = createStyles({
  root: {
    marginTop: '15px',
  },
  title: {
    paddingTop: '25px',
    paddingBottom: '25px',
    alignSelf: 'center',
  },
  button: {
    alignSelf: 'center',
    width: '95%',
    marginTop: '10px',
  },
  media: {
    height: '700px',
    backgroundImage: `url(${fiveCardStud})`,
    backgroundSize: 'contain',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    padding: '-5px',
  },
  mTop: {
    marginTop: '10px',
  },
  mRight: {
    marginRight: '10px',
  },
  mLeft: {
    marginLeft: '10px',
  },
  description: {
    height: '700px',
  },
});

const C = (props: WithStyles<typeof styles>) => {
  return (
    <Grid container justify="center" className={props.classes.root}>
      <Grid item xs={11}>
        <Card elevation={4} className={props.classes.title}>
          <Typography component="h2" variant="h3" align="center">
            🃏 Five Card Stud 🃏
          </Typography>
          <Typography component="p" variant="display1" color="textSecondary" style={{ fontSize: '1.5em' }}>
            <i>“I don’t play any two suited cards. I play any two non-suited cards. That way I’m drawing at two different flushes.” ~ Amarillo Slim</i>
          </Typography>
        </Card>
      </Grid>
      <Grid item xs={5}>
        <Card
          className={classNames(
            props.classes.mTop,
            props.classes.mRight,
            props.classes.description,
          )}
          elevation={4}
        >
          <CardContent>
            <Typography component="h4" variant="display2">
              $0.05/$0.10/$0.25/$0.50
            </Typography>
            <Typography component="p" variant="display1" align="left">
              <ul>
                <li>Two cards are dealt, one face up and one face down</li>
                <li>Betting starts with the player showing the lowest card face up and is known as the bring-in. Player can bet either the bring-in amount or the amount of the small bet</li>
                <li>3rd street is dealt face up and betting starts with the player showing the best hand. The player has the option to check</li>
                <li>4th and 5th street are played like 3rd street but the betting amount is the big bet</li>
              </ul>
            </Typography>
          </CardContent>
        </Card>
      </Grid>
      <Grid item xs={6}>
        <Card
          className={classNames(
            props.classes.media,
            props.classes.mTop,
            props.classes.mLeft,
          )}
          elevation={4}
        />
      </Grid>
    </Grid>
  );
};

export default withStyles(styles)(C);
