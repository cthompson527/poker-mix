import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { withStyles, createStyles, WithStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import handRankings from '../images/hand-rankings.jpg';
import classNames from 'classnames';

const styles = createStyles({
  root: {
    marginTop: '15px',
  },
  title: {
    paddingTop: '25px',
    paddingBottom: '25px',
    alignSelf: 'center',
  },
  button: {
    alignSelf: 'center',
    width: '95%',
    marginTop: '10px',
  },
  media: {
    height: '650px',
    backgroundImage: `url(${handRankings})`,
    backgroundSize: 'contain',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
  },
  mTop: {
    marginTop: '10px',
  },
  mRight: {
    marginRight: '10px',
  },
  mLeft: {
    marginLeft: '10px',
  },
  amounts: {
    height: '650px'
  }
});

const C = (props: WithStyles<typeof styles>) => {
  return (
    <Grid container className={props.classes.root} justify="center">
      <Grid item xs={11}>
        <Card elevation={4} className={props.classes.title}>
          <Typography component="h2" variant="h3" align="center">
            🃏 Poker Mixed Games 🃏
          </Typography>
          <Typography component="p" variant="display1" color="textSecondary">
            <i>Connect at corycs-pokermix.site</i>
          </Typography>
        </Card>
      </Grid>
      <Grid item xs={4}>
        <Card
          className={classNames(props.classes.mTop, props.classes.mRight, props.classes.amounts)}
          elevation={4}
        >
          <CardContent>
            <Typography component="p" variant="display2">
              <u>Amounts</u>
            </Typography>
            <Typography component="p" variant="display2">
              Blinds -- $0.05/$0.10
              <br />
              Antes -- $0.05
              <br />
              Bring In -- $0.10
              <br />
              Small Bet -- $0.25
              <br />
              Big Bet -- $0.50
              <br />
              Cap -- 3 Raises (4 Bets)
              <br />
              Bomb Pot -- $0.50
              <br />
              Blackjack -- $0.25
            </Typography>
          </CardContent>
        </Card>
      </Grid>
      <Grid item xs={7}>
        <Card
          className={classNames(
            props.classes.media,
            props.classes.mTop,
            props.classes.mLeft,
          )}
          elevation={4}
        />
      </Grid>
    </Grid>
  );
};

export default withStyles(styles)(C);
