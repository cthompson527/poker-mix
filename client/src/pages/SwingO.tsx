import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { withStyles, createStyles, WithStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import SwingO from '../images/swingo.jpg';
import classNames from 'classnames';

const styles = createStyles({
  root: {
    marginTop: '15px',
  },
  title: {
    paddingTop: '25px',
    paddingBottom: '25px',
    alignSelf: 'center',
  },
  button: {
    alignSelf: 'center',
    width: '95%',
    marginTop: '10px',
  },
  media: {
    height: '700px',
    backgroundImage: `url(${SwingO})`,
    backgroundSize: 'contain',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    padding: '-5px',
  },
  mTop: {
    marginTop: '10px',
  },
  mRight: {
    marginRight: '10px',
  },
  mLeft: {
    marginLeft: '10px',
  },
  description: {
    height: '700px',
  },
});

const C = (props: WithStyles<typeof styles>) => {
  return (
    <Grid container justify="center" className={props.classes.root}>
      <Grid item xs={11}>
        <Card elevation={4} className={props.classes.title}>
          <Typography component="h2" variant="h3" align="center">
            🃏 SwingO 🃏
          </Typography>
          <Typography component="p" variant="display1" color="textSecondary">
            <i>"If there weren’t luck involved I would win every time." ~ Phil Hellmuth</i>
          </Typography>
        </Card>
      </Grid>
      <Grid item xs={5}>
        <Card
          className={classNames(
            props.classes.mTop,
            props.classes.mRight,
            props.classes.description,
          )}
          elevation={4}
        >
          <CardContent>
            <Typography component="h4" variant="display2">
              $0.05/$0.10
            </Typography>
            <Typography component="p" variant="display1" align="left">
              <ul>
                <li>Each player is dealt five cards</li>
                <li>After a round of betting (Pot Limit), players remaining will reveal three of their cards simultaneously</li>
                <li>Each player now has *six* cards of information, five their own, plus ONE card from any of their opponent's hands</li>
                <li>Example: if Player One has A-A-5-5-8 and Player Two has 5-9-10, Player One will have a full house using A-A-5-5 and the 5 from Player Two. If Player Two folds, Player One will then only have two pair</li>
                <li>After a round of betting, a river card is dealt for all players to share</li>
                <li>Bet, then showdown</li>
              </ul>
            </Typography>
          </CardContent>
        </Card>
      </Grid>
      <Grid item xs={6}>
        <Card
          className={classNames(
            props.classes.media,
            props.classes.mTop,
            props.classes.mLeft,
          )}
          elevation={4}
        />
      </Grid>
    </Grid>
  );
};

export default withStyles(styles)(C);
