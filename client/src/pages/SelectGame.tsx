import React, { useState } from 'react';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { subscribeAsClient, selectGame, selectRandomGame } from '../websocket';
import { withStyles, createStyles, WithStyles } from '@material-ui/core/styles';

const styles = createStyles({
  root: {
    marginTop: '25px',
    marginBottom: '25px',
  },
  button: {
    alignSelf: 'center',
    width: '95%',
    marginTop: '10px',
  },
});

const connectToWS = () => subscribeAsClient(null);

const C = (props: WithStyles<typeof styles>) => {
  const [connected, setConnected] = useState(false);
  if (!connected) {
    console.log('connecting');
    connectToWS();
    setConnected(true);
  }
  return (
    <Grid container className={props.classes.root}>
      <Grid item xs={12}>
        <Typography component="h2" variant="h3" align="center">
          🃏 Games 🃏
        </Typography>
      </Grid>
      <Grid item xs={12}>
        <Button
          fullWidth
          color="primary"
          variant="contained"
          onClick={() => selectGame('Holdem')}
          className={props.classes.button}
        >
          Texas Hold'em
        </Button>
        <Button
          fullWidth
          color="primary"
          variant="contained"
          onClick={() => selectGame('Holdem Bomb Pot')}
          className={props.classes.button}
        >
          Texas Hold'em Bomb Pot
        </Button>
        <Button
          fullWidth
          color="primary"
          variant="contained"
          onClick={() => selectGame(`Double Flop Hold'em`)}
          className={props.classes.button}
        >
          Double Flop Texas Hold'em
        </Button>
        <Button
          fullWidth
          color="primary"
          variant="contained"
          onClick={() => selectGame('7 Card Stud')}
          className={props.classes.button}
        >
          Seven Card Stud
        </Button>
        <Button
          fullWidth
          color="primary"
          variant="contained"
          onClick={() => selectGame('7 Card Stud Hi/Lo')}
          className={props.classes.button}
        >
          Seven Card Stud Hi-Lo
        </Button>
        <Button
          fullWidth
          color="primary"
          variant="contained"
          onClick={() => selectGame('Razz')}
          className={props.classes.button}
        >
          Razz
        </Button>
        <Button
          fullWidth
          color="primary"
          variant="contained"
          onClick={() => selectGame('SwingO')}
          className={props.classes.button}
        >
          SwingO
        </Button>
        <Button
          fullWidth
          color="primary"
          variant="contained"
          onClick={() => selectGame('5 Card Stud')}
          className={props.classes.button}
        >
          Five Card Stud
        </Button>
        <Button
          fullWidth
          color="primary"
          variant="contained"
          onClick={() => selectGame('Omaha')}
          className={props.classes.button}
        >
          Omaha
        </Button>
        <Button
          fullWidth
          color="primary"
          variant="contained"
          onClick={() => selectGame('Omaha Hi/Lo')}
          className={props.classes.button}
        >
          Omaha Hi-Lo
        </Button>
        <Button
          fullWidth
          color="primary"
          variant="contained"
          onClick={() => selectGame('Big O')}
          className={props.classes.button}
        >
          Big O
        </Button>
        <Button
          fullWidth
          color="primary"
          variant="contained"
          onClick={() => selectGame('Crazy Pineapple')}
          className={props.classes.button}
        >
          Crazy Pineapple
        </Button>
        <Button
          fullWidth
          color="primary"
          variant="contained"
          onClick={() => selectGame('2-7 Triple Draw')}
          className={props.classes.button}
        >
          2-7 Triple Draw
        </Button>
        <Button
          fullWidth
          color="primary"
          variant="contained"
          onClick={() => selectGame('No Dealer Blackjack')}
          className={props.classes.button}
        >
          No Dealer Blackjack
        </Button>
        <Button
          fullWidth
          color="primary"
          variant="contained"
          onClick={() => selectRandomGame()}
          className={props.classes.button}
        >
          Random
        </Button>
        <Button
          fullWidth
          color="primary"
          variant="contained"
          onClick={() => selectGame('Home')}
          className={props.classes.button}
        >
          Home
        </Button>
      </Grid>
    </Grid>
  );
};

export default withStyles(styles)(C);
