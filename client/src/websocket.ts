import openSocket from 'socket.io-client';

interface IRoutes {
  [name: string]: string;
}

const nameToRoute: IRoutes = {
  Holdem: '/tv/txholdem',
  'Holdem Bomb Pot': '/tv/txholdembombpot',
  '7 Card Stud': '/tv/7cardstud',
  '7 Card Stud Hi/Lo': '/tv/7cardstud8',
  '5 Card Stud': '/tv/5cardstud',
  Razz: '/tv/razz',
  Omaha: '/tv/omaha',
  'Omaha Hi/Lo': '/tv/omaha8',
  'Big O': '/tv/bigo',
  'Crazy Pineapple': '/tv/crazypineapple',
  '2-7 Triple Draw': '/tv/twoseventriple',
  'No Dealer Blackjack': '/tv/blackjack',
  'Home': '/tv/home',
  "Double Flop Hold'em": '/tv/doubleflop',
  'SwingO': '/tv/swingo',
};

let clientSocket: SocketIOClient.Socket;
const subscribeAsClient = (cb: any) => {
  clientSocket = openSocket('/client');
};

const selectGame = (game: string) => {
  console.log('sending change game');
  clientSocket.emit('change game', game);
};

const selectRandomGame = () => {
  const numOfGames = Object.keys(nameToRoute).length;
  const randomGame = Math.floor(Math.random() * numOfGames);
  clientSocket.emit('change game', Object.keys(nameToRoute)[randomGame]);
};

let serverSocket: SocketIOClient.Socket;
const subscribeAsServer = (h: any) => {
  const history = h;
  let timeout = setTimeout(() => {console.log('initial timeout')}, 0);
  console.log(history);
  serverSocket = openSocket('/server');
  serverSocket.on('change game', (route: string) => {
    clearTimeout(timeout);
    console.log(route);
    console.log(nameToRoute);
    history.push(nameToRoute[route]);
    timeout = setTimeout(() => history.push(nameToRoute['Home']), 300000);
  });
};

export { subscribeAsClient, selectGame, subscribeAsServer, selectRandomGame };
