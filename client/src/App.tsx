import React, { Component } from 'react';
import './App.css';
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Link,
  Redirect,
} from 'react-router-dom';
import SelectGame from './pages/SelectGame';
import TVLandingPage from './pages/TVLandingPage';
import TexasHoldem from './pages/TexasHoldem';
import TexasHoldemBombPot from './pages/TexasHoldemBombPot';
import ServerWS from './pages/ServerWS';
import SevenCardStud from './pages/SevenCardStud';
import SevenCardStudHiLo from './pages/SevenCardStudHiLo';
import Razz from './pages/Razz';
import FiveCardStud from './pages/FiveCardStud';
import Omaha from './pages/Omaha';
import OmahaHiLo from './pages/OmahaHiLo';
import BigO from './pages/BigO';
import CrazyPineapple from './pages/CrazyPineapple';
import TwoToSeven from './pages/TwoToSeven';
import NoDealerBlackjack from './pages/NoDealerBlackjack';
import DoubleFlopHoldem from './pages/DoubleFlopHoldem';
import SwingO from './pages/SwingO';

class App extends Component {
  state = {
    timestamp: '',
  };
  render() {
    return (
      <div className="App">
        <Router>
          <Route path="/" exact render={() => <Redirect to="/select" />} />
          <Route path="/select" component={SelectGame} />
          <Route path="/tv" component={ServerWS} />
          <Route path="/tv/home" component={TVLandingPage} />
          <Route path="/tv/txholdem" component={TexasHoldem} />
          <Route path="/tv/txholdembombpot" component={TexasHoldemBombPot} />
          <Route path="/tv/7cardstud" component={SevenCardStud} />
          <Route path="/tv/7cardstud8" component={SevenCardStudHiLo} />
          <Route path="/tv/razz" component={Razz} />
          <Route path="/tv/5cardstud" component={FiveCardStud} />
          <Route path="/tv/omaha" component={Omaha} />
          <Route path="/tv/omaha8" component={OmahaHiLo} />
          <Route path="/tv/bigo" component={BigO} />
          <Route path="/tv/crazypineapple" component={CrazyPineapple} />
          <Route path="/tv/twoseventriple" component={TwoToSeven} />
          <Route path="/tv/blackjack" component={NoDealerBlackjack} />
          <Route path="/tv/doubleflop" component={DoubleFlopHoldem} />
          <Route path="/tv/swingo" component={SwingO} />
        </Router>
      </div>
    );
  }
}

export default App;
