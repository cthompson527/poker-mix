import socket, { Socket } from 'socket.io';

const io = socket();

// io.on('connection', (socket: Socket) => {
//   console.log('test');
// });
const serverNamespace = io.of('/server');
serverNamespace.on('connection', (socket: Socket) => {
  // console.log('server connected');
});

const clientNamespace = io.of('/client');
clientNamespace.on('connection', (socket: Socket) => {
  // console.log('client connected');
  socket.on('disconnect', () => console.log('client disconnected'));
  socket.on('change game', (game: string) => {
    // console.log(game);
    serverNamespace.emit('change game', game);
  });
});

io.listen(4000);
